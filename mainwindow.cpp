#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    serial = new QSerialPort(this);
    connect(serial, &QSerialPort::readyRead, this, &MainWindow::readData);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    ui->plainTextEdit->clear();
}

void MainWindow::on_plainTextEdit_textChanged()
{

}

void MainWindow::on_pushButton_pressed()
{
    serial->write(ui->plainTextEdit->toPlainText().toStdString().c_str());
    ui->plainTextEdit->clear();
}

void MainWindow::on_pushButton_2_clicked()
{
    serial->setBaudRate(QSerialPort::Baud9600);
    serial->setPortName(ui->lineEdit->text());
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::TwoStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    serial->open(QIODevice::ReadWrite);
    updateKeys();
}

void MainWindow::on_pushButton_3_clicked()
{
    serial->close();
    updateKeys();
}

void MainWindow::readData()
{
    QByteArray data = serial->readAll();
    QTextCursor prev_cursor = ui->textBrowser->textCursor();
    ui->textBrowser->moveCursor(QTextCursor::End);
    ui->textBrowser->insertPlainText(data);
    ui->textBrowser->setTextCursor(prev_cursor);
}

void MainWindow::updateKeys()
{
    if(serial->isOpen())
    {
        ui->lineEdit->setEnabled(false);
        ui->label->setText("Connected");
        ui->pushButton->setEnabled(true);
        ui->pushButton_2->setEnabled(false);
        ui->pushButton_3->setEnabled(true);
        ui->pushButton_cq->setEnabled(true);
        ui->pushButton_ans->setEnabled(true);
        ui->lineEdit_mySign->setEnabled(false);
        ui->lineEdit_sign->setEnabled(true);
        ui->lineEdit_ditTime->setEnabled(false);
        ui->pushButton_k->setEnabled(true);
        ui->pushButton_sk->setEnabled(true);
    }
    else
    {
        ui->lineEdit->setEnabled(true);
        ui->label->setText("Not Connected");
        ui->pushButton->setEnabled(false);
        ui->pushButton_2->setEnabled(true);
        ui->pushButton_3->setEnabled(false);
        ui->pushButton_cq->setEnabled(false);
        ui->pushButton_ans->setEnabled(false);
        ui->lineEdit_mySign->setEnabled(true);
        ui->lineEdit_sign->setEnabled(false);
        ui->lineEdit_ditTime->setEnabled(true);
        ui->pushButton_k->setEnabled(false);
        ui->pushButton_sk->setEnabled(false);
    }
}

void MainWindow::on_pushButton_cq_clicked()
{
    QString toSend;

    if(ui->checkBox_dxMode->isChecked())
        toSend = "cq cq cq " + ui->lineEdit_mySign->text() + " " + ui->lineEdit_mySign->text() + " " + ui->lineEdit_mySign->text() + " k";
    else
        toSend = "cq " + ui->lineEdit_mySign->text() + " " + ui->lineEdit_mySign->text() + " k";

    serial->write(toSend.toStdString().c_str());
}

void MainWindow::on_pushButton_k_clicked()
{
    QString toSend = ui->plainTextEdit->toPlainText() + " " + ui->lineEdit_sign->text() + " de " + ui->lineEdit_mySign->text() + " k";
    ui->plainTextEdit->clear();
    serial->write(toSend.toStdString().c_str());
}

void MainWindow::on_pushButton_ans_clicked()
{
    QString toSend;

    if(ui->checkBox_dxMode->isChecked())
        toSend = ui->lineEdit_sign->text() + " " + ui->lineEdit_sign->text() + " de " + ui->lineEdit_mySign->text() + " " + ui->lineEdit_mySign->text() + " " + ui->lineEdit_mySign->text() + " kn";
    else
        toSend = ui->lineEdit_sign->text() + " de " + ui->lineEdit_mySign->text() + " " + ui->lineEdit_mySign->text() + " kn";

    serial->write(toSend.toStdString().c_str());
}

void MainWindow::on_pushButton_sk_clicked()
{
    QString toSend = ui->plainTextEdit->toPlainText() + " " + ui->lineEdit_sign->text() + " de " + ui->lineEdit_mySign->text() + " sk";
    ui->plainTextEdit->clear();
    serial->write(toSend.toStdString().c_str());
}
