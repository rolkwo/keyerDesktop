#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QtSerialPort/QtSerialPort>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_plainTextEdit_textChanged();

    void on_pushButton_pressed();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void readData();

    void on_pushButton_cq_clicked();

    void on_pushButton_k_clicked();

    void on_pushButton_ans_clicked();

    void on_pushButton_sk_clicked();

private:
    Ui::MainWindow *ui;

    QSerialPort *serial;

    void updateKeys();
};

#endif // MAINWINDOW_H
